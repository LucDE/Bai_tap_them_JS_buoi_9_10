const regex_username = /^[0-9]{4,6}$/;
const regex_name = /[a-zA-Z\s\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u024F]/;
const regex_email = /^([\w\.])+\@([\w\-])+\.([a-zA-Z]{2,4})(\.[a-zA-Z]{2,4})?$/;
const regex_date =
  /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/;
const regex_check_A_Z = /[A-Z]{1,}/;
const regex_check_0_9 = /[0-9]{1,}/;
const regex_check_special_character =
  /[\`\~\!\@\#\%\&\>\<\:\;\,\_\=\"\'-.*+?^${}()|/[\]\\]{1,}/;
//Username validate
function checkUserName(input) {
  var input_trim = input.trim();
  if (input_trim.match(regex_username)) {
    alert_user_name.classList.add("d-none");
    alert_user_name.innerHTML = ``;
    username.classList.remove("wrong_input");
    return true;
  } else {
    alert_user_name.classList.remove("d-none");
    alert_user_name.innerHTML = `<p> Vui lòng nhập từ 4-6 chữ số </p>`;
    user_name.classList.add("wrong_input");
    return false;
  }
}
//Employee name
function checkName(input) {
  //regex for unicode, latin
  var input_trim = input.trim();
  if (input_trim.match(regex_name)) {
    alert_employee_name.classList.add("d-none");
    alert_employee_name.innerHTML = ``;
    employee_name.classList.remove("wrong_input");
    return true;
  } else {
    alert_employee_name.classList.remove("d-none");
    alert_employee_name.innerHTML = `<p> Vui lòng nhập tên bao gồm chữ cái </p>`;
    employee_name.classList.add("wrong_input");
    return false;
  }
}
//Email
function checkEmail(input) {
  var input_trim = input.trim();
  if (input_trim.match(regex_email)) {
    alert_email.classList.add("d-none");
    alert_email.innerHTML = ``;
    email.classList.remove("wrong_input");
    return true;
  } else {
    alert_email.classList.remove("d-none");
    alert_email.innerHTML = `<p> Vui lòng nhập email đúng định dạng </p>`;
    email.classList.add("wrong_input");
    return false;
  }
}
//password
function checkPassWord(input) {
  var input_trim = input.trim();
  var arr_spec_char = input_trim.match(regex_check_special_character);
  var arr_0_9 = input_trim.match(regex_check_0_9);
  var arr_A_Z = input_trim.match(regex_check_A_Z);
  if (input_trim.length >= 6 && input_trim.length <= 8) {
    if (arr_spec_char !== null && arr_0_9 !== null && arr_A_Z !== null) {
      alert_password.classList.add("d-none");
      alert_password.innerHTML = ``;
      pass_word.classList.remove("wrong_input");
      return true;
    } else {
      alert_password.classList.remove("d-none");
      alert_password.innerHTML = ` <p> Độ dài: 6-10 ký tự </p>
      <p> Mật khẩu chứa ít nhất 1 ký tự số, 1 ký tự chữ in hoa, 1 ký tự đặc biệt  </p>`;
      pass_word.classList.add("wrong_input");
      return false;
    }
  } else {
    alert_password.classList.remove("d-none");
    alert_password.innerHTML = ` <p> Độ dài: 6-10 ký tự </p>
    <p> Mật khẩu chứa ít nhất 1 ký tự số, 1 ký tự chữ in hoa, 1 ký tự đặc biệt  </p>`;
    pass_word.classList.add("wrong_input");
    return false;
  }
}
//date
function checkDate(input) {
  var input_trim = input.trim();
  if (input_trim.match(regex_date)) {
    alert_date.classList.add("d-none");
    alert_date.innerHTML = ``;
    date.classList.remove("wrong_input");
    return true;
  } else {
    alert_date.classList.remove("d-none");
    alert_date.innerHTML = `<p> Vui lòng nhập đúng định dạng: tháng/ngày/năm </p>`;
    date.classList.add("wrong_input");
    return false;
  }
}
//salary
function checkSalary(input) {
  var input_trim = input.trim() * 1;
  if (input_trim < 1000000 || input_trim > 20000000) {
    alert_salary.classList.remove("d-none");
    alert_salary.innerHTML = `<p> Vui lòng nhập đúng số lương </p>`;
    salary.classList.add("wrong_input");

    return false;
  } else {
    alert_salary.classList.add("d-none");
    alert_salary.innerHTML = ``;
    salary.classList.remove("wrong_input");
    return true;
  }
}
//position
function checkPosition(input) {
  if (input != "") {
    alert_position.classList.add("d-none");
    alert_position.innerHTML = ``;
    position.classList.remove("wrong_input");
    return true;
  } else {
    alert_position.classList.remove("d-none");
    alert_position.innerHTML = `<p> Vui lòng chọn chức vụ </p>`;
    position.classList.add("wrong_input");
    return false;
  }
}
//work time
function checkWorkTime(input) {
  var input_trim = input.trim() * 1;
  if (input_trim < 80 || input_trim > 200) {
    alert_worktime.classList.remove("d-none");
    alert_worktime.innerHTML = `<span> Vui lòng nhập đúng số giờ làm </span>`;
    work_time.classList.add("wrong_input");
    return false;
  } else {
    alert_worktime.classList.add("d-none");
    alert_worktime.innerHTML = ``;
    work_time.classList.remove("wrong_input");
    return true;
  }
}
//Input return blank
function returnBlank() {
  user_name.value = "";
  employee_name.value = "";
  email.value = "";
  pass_word.value = "";
  date.value = "";
  salary.value = "";
  position.value = "";
  work_time.value = "";
}
// alert return none
function returnNoneAlert() {
  username.classList.remove("wrong_input");
  employee_name.classList.remove("wrong_input");
  pass_word.classList.remove("wrong_input");
  email.classList.remove("wrong_input");
  date.classList.remove("wrong_input");
  salary.classList.remove("wrong_input");
  position.classList.remove("wrong_input");
  work_time.classList.remove("wrong_input");
  alert_user_name.classList.add("d-none");
  alert_user_name.innerHTML = ``;
  alert_employee_name.classList.add("d-none");
  alert_employee_name.innerHTML = ``;
  alert_email.classList.add("d-none");
  alert_email.innerHTML = ``;
  alert_password.classList.add("d-none");
  alert_password.innerHTML = ``;
  alert_date.classList.add("d-none");
  alert_date.innerHTML = ``;
  alert_position.classList.add("d-none");
  alert_position.innerHTML = ``;
  alert_salary.classList.add("d-none");
  alert_salary.innerHTML = ``;
  alert_worktime.classList.add("d-none");
  alert_worktime.innerHTML = ``;
}
//check if username input add duplicated
function checkUsernameAdd(input) {
  var check_count = 0;
  if (array_employee.length > 0) {
    for (i = 0; i <= array_employee.length - 1; i++) {
      if (array_employee[i].user_name_attribute === input) {
        check_count++;
      }
    }
    if (check_count >= 1) {
      alert_duplicate_user_name.innerHTML = `<p> Username đã tồn tại </p>`;
      alert_duplicate_user_name.classList.remove("d-none");
      user_name.classList.add("duplicate_input");
      return true;
    } else {
      alert_duplicate_user_name.classList.add("d-none");
      alert_duplicate_user_name.innerHTML = ``;
      username.classList.remove("duplicate_input");
      return false;
    }
  } else {
    return false;
  }
}
//check if username input update dulicated
function checkUserNameUpdate(input) {
  var check_count = 0;
  for (j = 0; j <= username_update_check_list.length - 1; j++) {
    if (username_update_check_list[j] === input) {
      check_count++;
    }
  }
  if (check_count >= 1) {
    alert_duplicate_user_name.innerHTML = `<p> Username đã tồn tại </p>`;
    alert_duplicate_user_name.classList.remove("d-none");
    user_name.classList.add("duplicate_input");
    return true;
  } else {
    alert_duplicate_user_name.classList.add("d-none");
    alert_duplicate_user_name.innerHTML = ``;
    username.classList.remove("duplicate_input");
    return false;
  }
}
