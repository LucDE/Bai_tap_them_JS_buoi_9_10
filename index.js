//Global variable
const login_part = document.querySelector("#login_part");
const table_part = document.querySelector("#table_part");
const array_employee = [];
const user_name = document.querySelector("#username");
const employee_name = document.querySelector("#employee_name");
const email = document.querySelector("#email");
const pass_word = document.querySelector("#password");
const date = document.querySelector("#date");
const salary = document.querySelector("#salary");
const position = document.querySelector("#position");
const work_time = document.querySelector("#work_time");
const table_1 = document.querySelector("#table_1");
const table_2 = document.querySelector("#table_2");
const type_search_list = document.querySelector("#type_search_list");
const add_btn = document.querySelector("#add_btn");
const update_btnn = document.querySelector("#update_btn");
const search_input = document.querySelector("#search_input");
var index_update;
var currentTD_list;
var username_update_check_list = [];
var count = 0; //Dùng để làm định danh (stt) nhưng chưa sử dụng
//Validate var
const alert_user_name = document.querySelector("#username_alert");
const alert_duplicate_user_name = document.querySelector("#username_duplicate");
const alert_number_of_type = document.querySelector("#alert_number_of_type");
const alert_employee_name = document.querySelector("#employee_name_alert");
const alert_email = document.querySelector("#email_alert");
const alert_password = document.querySelector("#password_alert");
const alert_date = document.querySelector("#date_alert");
const alert_salary = document.querySelector("#salary_alert");
const alert_position = document.querySelector("#position_alert");
const alert_worktime = document.querySelector("#worktime_alert");
//Layout
function openLogin() {
  table_part.classList.add("blur");
  login_part.classList.remove("d-none");
  update_btnn.classList.add("d-none");
}
function closeLogin() {
  table_part.classList.remove("blur");
  login_part.classList.add("d-none");
  add_btn.classList.remove("d-none");
  update_btnn.classList.remove("d-none");
  returnBlank();
  returnNoneAlert();
}
//Function
//Lớp đối tượng Nhân viên
function EmployeeObject(
  _username,
  _employee_name,
  _email,
  _date,
  _position,
  _total_salary,
  _type,
  _stt,
  _salary,
  _work_time,
  _pass_work
) {
  this.user_name_attribute = _username;
  this.employee_name_attribute = _employee_name;
  this.email_attribute = _email;
  this.date_attribute = _date;
  this.position_attribute = _position;
  this.total_salary_attribute = _total_salary;
  this.type_of_employee = _type;
  this.stt = _stt;
  this.salary_attribute = _salary;
  this.work_time_attribute = _work_time;
  this.password_attribute = _pass_work;
}
//Thêm nhân viên
function addEmployee() {
  var attribute1 = user_name.value;
  var attribute2 = employee_name.value;
  var attribute3 = email.value;
  var attribute4 = date.value;
  var attribute5 = position.value;
  var attribute9 = salary.value;
  var attribute10 = work_time.value;
  //tổng lương
  var attribute6 = calculateSalary(attribute9, attribute5);
  //loại nhân viên
  var attribute7 = calculateType(attribute10);
  var attribute8 = count;
  var attribute11 = pass_word.value;
  var boolean_username = checkUserName(attribute1);
  var boolean_employee_name = checkName(attribute2);
  var boolean_email = checkEmail(attribute3);
  var boolean_password = checkPassWord(attribute11);
  var boolean_date = checkDate(attribute4);
  var boolean_salary = checkSalary(attribute9);
  var boolean_position = checkPosition(attribute5);
  var boolean_worktime = checkWorkTime(attribute10);
  var boolean_username_duplicate = checkUsernameAdd(attribute1);
  if (
    boolean_username === true &&
    boolean_employee_name === true &&
    boolean_email === true &&
    boolean_password === true &&
    boolean_date === true &&
    boolean_salary === true &&
    boolean_position === true &&
    boolean_worktime === true &&
    boolean_username_duplicate === false
  ) {
    var employee = new EmployeeObject(
      attribute1,
      attribute2,
      attribute3,
      attribute4,
      attribute5,
      attribute6,
      attribute7,
      attribute8,
      attribute9,
      attribute10,
      attribute11
    );
    array_employee.push(employee);
    // console.log("array_employee: ", array_employee);
    // trả về input rổng, đang tắt để đỡ phải nhập nhiểu lần
    // returnBlank();
    var TR = document.createElement("tr");
    var TD_list = [];
    for (i = 0; i <= 6; i++) {
      var td = document.createElement("td");
      td.style.border = "1px solid black";
      TD_list.push(td);
      TR.appendChild(TD_list[i]);
    }
    for (j = array_employee.length - 1; j <= array_employee.length - 1; j++) {
      TD_list[0].innerHTML = `${array_employee[j].user_name_attribute}`;
      TD_list[1].innerHTML = `${array_employee[j].employee_name_attribute}`;
      TD_list[2].innerHTML = `${array_employee[j].email_attribute}`;
      TD_list[3].innerHTML = `${array_employee[j].date_attribute}`;
      TD_list[4].innerHTML = `${array_employee[j].position_attribute}`;
      TD_list[5].innerHTML = `${array_employee[j].total_salary_attribute}`;
      TD_list[6].innerHTML = `${array_employee[j].type_of_employee}`;
    }
    var td7 = document.createElement("td");
    td7.style.border = "1px solid black";
    td7.style.padding = "5px";
    var update_btn = document.createElement("button");
    update_btn.innerHTML = `Cập nhật`;
    var delete_btn = document.createElement("button");
    delete_btn.innerHTML = `Xóa`;
    delete_btn.className = "btn bg-danger text-white ml-2";
    update_btn.className = "btn bg-success text-white";
    td7.appendChild(update_btn);
    td7.appendChild(delete_btn);
    TR.appendChild(td7);
    table_1.appendChild(TR);
    //Xóa nhân viên
    delete_btn.onclick = function () {
      // console.log("xóa nhân viên ");
      //trong hàm này this chính là cái nút button/ không còn trỏ vào cái nào khác
      var currentTD = this.parentNode;
      var currentTR = currentTD.parentNode;
      //currenTD_list là mảng chứa toàn bộ thẻ <td> của thẻ <tr> có chứa button
      var CurrentTD_list = currentTR.querySelectorAll("td");
      //tìm username của thẻ <tr>
      var current_username = CurrentTD_list[0].innerText;
      // console.log("currtentTD_list: ", CurrentTD_list);
      // console.log("current_username: ", current_username);
      //Tìm đối tượng có username trùng với username của thẻ <td>
      //findIndex trả về vị trí đầu tiên tìm thấy
      var index_delete = array_employee.findIndex(function (obj) {
        if (obj.user_name_attribute === current_username) {
          return obj;
        }
      });
      // console.log("index_delete: ", index_delete);
      //Xóa đối tượng trong mảng
      array_employee.splice(index_delete, 1);
      // console.log("  array_employee: ", array_employee);
      // Xóa thẻ <tr> trong HTML
      var currentTable = currentTR.parentNode;
      currentTable.removeChild(currentTR);
    };
    //Mở phần cập nhật nhân viên
    update_btn.onclick = function () {
      console.log("Cập nhật nhân viên");
      add_btn.classList.add("d-none");
      openLogin();
      update_btnn.classList.remove("d-none");
      var currentTD = this.parentNode;
      var currentTR = currentTD.parentNode;
      currentTD_list = currentTR.querySelectorAll("td");
      //Tìm username của thẻ <tr>
      var current_username = currentTD_list[0].innerText;
      //Trả về biến index_update ra Global
      index_update = array_employee.findIndex(function (obj) {
        if (obj.user_name_attribute === current_username) {
          return obj;
        }
      });
      //Trả về list so sánh với user_name_update
      var currentTR_list = table_1.querySelectorAll("tr");
      console.log("currentTR_list: ", currentTR_list);
      var current_username_list = [];
      for (i = 0; i <= currentTR_list.length - 1; i++) {
        var currentTD_username =
          currentTR_list[i].querySelectorAll("td")[0].innerHTML;
        current_username_list.push(currentTD_username);
      }
      console.log("current_username_list: ", current_username_list);
      current_username_list.filter(function (itemm) {
        if (itemm != current_username) {
          username_update_check_list.push(itemm);
        }
      });
      console.log("username_update_check_list: ", username_update_check_list);
      //Hiển thị lại thông tin người dùng nhập
      var employeee = array_employee.find(function (obj) {
        if (obj.user_name_attribute === current_username) {
          return obj;
        }
      });
      user_name.value = employeee.user_name_attribute;
      employee_name.value = employeee.employee_name_attribute;
      email.value = employeee.email_attribute;
      date.value = employeee.date_attribute;
      position.value = employeee.position_attribute;
      salary.value = employeee.salary_attribute;
      work_time.value = employeee.work_time_attribute;
      pass_word.value = employeee.password_attribute;
    };
    //chưa dùng đến count
    count++;
  }
}
//Cập nhật nhân viên
function updateEmployee() {
  //lấy biến global index_update: vị trí của object trong mảng
  var new_attribute1 = user_name.value;
  var new_attribute2 = employee_name.value;
  var new_attribute3 = email.value;
  var new_attribute4 = date.value;
  var new_attribute5 = position.value;
  var new_attribute9 = salary.value;
  var new_attribute10 = work_time.value;
  //tổng lương
  var new_attribute6 = calculateSalary(new_attribute9, new_attribute5);
  //loại nhân viên
  var new_attribute7 = calculateType(new_attribute10);
  //count mới không được thay đổi, bằng định danh ID (là stt)
  var new_attribute8 = index_update;
  var new_attribute11 = pass_word.value;
  var boolean_username = checkUserName(new_attribute1);
  var boolean_employee_name = checkName(new_attribute2);
  var boolean_email = checkEmail(new_attribute3);
  var boolean_password = checkPassWord(new_attribute11);
  var boolean_date = checkDate(new_attribute4);
  var boolean_salary = checkSalary(new_attribute9);
  var boolean_position = checkPosition(new_attribute5);
  var boolean_worktime = checkWorkTime(new_attribute10);
  var boolean_username_duplicate = checkUserNameUpdate(new_attribute1);
  if (
    boolean_username === true &&
    boolean_employee_name === true &&
    boolean_email === true &&
    boolean_password === true &&
    boolean_date === true &&
    boolean_salary === true &&
    boolean_position === true &&
    boolean_worktime === true &&
    boolean_username_duplicate === false
  ) {
    var update_employee = new EmployeeObject(
      new_attribute1,
      new_attribute2,
      new_attribute3,
      new_attribute4,
      new_attribute5,
      new_attribute6,
      new_attribute7,
      new_attribute8,
      new_attribute9,
      new_attribute10,
      new_attribute11
    );
    array_employee.splice(index_update, 1, update_employee);
    for (j = index_update; j <= index_update; j++) {
      currentTD_list[0].innerHTML = `${array_employee[j].user_name_attribute}`;
      currentTD_list[1].innerHTML = `${array_employee[j].employee_name_attribute}`;
      currentTD_list[2].innerHTML = `${array_employee[j].email_attribute}`;
      currentTD_list[3].innerHTML = `${array_employee[j].date_attribute}`;
      currentTD_list[4].innerHTML = `${array_employee[j].position_attribute}`;
      currentTD_list[5].innerHTML = `${array_employee[j].total_salary_attribute}`;
      currentTD_list[6].innerHTML = `${array_employee[j].type_of_employee}`;
    }
    username_update_check_list = [];
    console.log("array_employee: ", array_employee);
    closeLogin();
  }
}
//Tìm nhân viên
function findType() {
  console.log("tìm nhân viên");
  table_1.classList.add("d-none");
  type_search_list.classList.remove("d-none");
  var TR_table_2 = table_2.querySelectorAll("tr");
  console.log("TR_table_2: ", TR_table_2);
  for (i = 0; i <= TR_table_2.length - 1; i++) {
    table_2.removeChild(TR_table_2[i]);
  }
  var type_input = search_input.value;
  var type_input_trim = type_input.trim();
  if (type_input_trim === "") {
    alert_number_of_type.innerHTML = `<p> Vui lòng nhập loại nhân viên</p>`;
  } else {
    var type_input_trim_uppercase = type_input_trim.toUpperCase();
    var TR_list = table_1.querySelectorAll("tr");
    var type_of_employee = [];
    var index_match = [];
    for (i = 0; i <= TR_list.length - 1; i++) {
      var TD_list = TR_list[i].querySelectorAll("td");
      var TD_6 = TD_list[6].innerHTML;
      type_of_employee.push(TD_6);
    }
    // console.log("type_of_employee: ", type_of_employee);
    type_of_employee.filter(function (item, index) {
      var item_uppercase = item.toUpperCase();
      if (item_uppercase === type_input_trim_uppercase) {
        index_match.push(index);
      }
    });
    if (index_match.length > 0) {
      for (n = 0; n <= index_match.length - 1; n++) {
        let TR = document.createElement("tr");
        let TD_list = [];
        for (i = 0; i <= 6; i++) {
          let td = document.createElement("td");
          td.style.border = "1px solid black";
          TD_list.push(td);
          TR.appendChild(TD_list[i]);
        }
        for (j = 0; j <= 0; j++) {
          TD_list[0].innerHTML = `${
            array_employee[index_match[n]].user_name_attribute
          }`;
          TD_list[1].innerHTML = `${
            array_employee[index_match[n]].employee_name_attribute
          }`;
          TD_list[2].innerHTML = `${
            array_employee[index_match[n]].email_attribute
          }`;
          TD_list[3].innerHTML = `${
            array_employee[index_match[n]].date_attribute
          }`;
          TD_list[4].innerHTML = `${
            array_employee[index_match[n]].position_attribute
          }`;
          TD_list[5].innerHTML = `${
            array_employee[index_match[n]].total_salary_attribute
          }`;
          TD_list[6].innerHTML = `${
            array_employee[index_match[n]].type_of_employee
          }`;
        }
        alert_number_of_type.innerHTML = `<p> Có ${index_match.length} nhân viên thuộc xếp loại ${type_input_trim} </p>`;
        //Chỗ này nếu rảnh thêm chức năng tìm xong cập nhật + xóa
        table_2.appendChild(TR);
      }
    } else {
      alert_number_of_type.innerHTML = `<p> Không nhân viên nào thuộc xếp loại ${type_input_trim} </p>`;
    }
  }
  search_input.value = "";
  search_input.focus();
}
// Hàm tính lương
function calculateSalary(salary, position) {
  var total_salary;
  if (position === "Giám đốc") {
    total_salary = salary * 3;
  } else if (position === "Trưởng phòng") {
    total_salary = salary * 2;
  } else {
    total_salary = salary * 1;
  }
  return `${total_salary.toLocaleString()} VNĐ`;
}
//Hàm xếp loại nhân viên
function calculateType(time_working) {
  var type;
  if (time_working >= 192) {
    type = "Xuất xắc";
  } else if (time_working >= 176) {
    type = "Giỏi";
  } else if (time_working >= 160) {
    type = "Khá";
  } else {
    type = "Trung Bình";
  }
  return type;
}
//Hàm quay trở lại list ban đầu
function backToList() {
  var TR_table_2 = table_2.querySelectorAll("tr");
  console.log("TR_table_2: ", TR_table_2);
  for (i = 0; i <= TR_table_2.length - 1; i++) {
    table_2.removeChild(TR_table_2[i]);
  }
  console.log("TR_table_2: ", TR_table_2);
  type_search_list.classList.add("d-none");
  table_1.classList.remove("d-none");
}
